package br.ufc.quixada.dadm.variastelas.service;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.Objects;

class UtilService {

    String getContactName(Intent i){
        return (String) Objects.requireNonNull(i.getExtras()).get("nome");
    }

    String getContactTel(Intent i){
        return (String) Objects.requireNonNull(i.getExtras()).get("telefone");
    }

    String getContactEnd(Intent i){
        return (String) Objects.requireNonNull(i.getExtras()).get("endereco");
    }

    int getContactId(Intent i){
        return (int) Objects.requireNonNull(i.getExtras()).get("id");
    }

    void makeToast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
