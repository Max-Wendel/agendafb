package br.ufc.quixada.dadm.variastelas;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import br.ufc.quixada.dadm.variastelas.dao.ContatoDAO;
import br.ufc.quixada.dadm.variastelas.entity.Contato;

@Database(entities = {Contato.class}, version = 1, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {

   private static AppDataBase instance;

   public abstract ContatoDAO contatoDAO();

   private static void initInstance(Context context){
       instance = Room.databaseBuilder(context.getApplicationContext(),
               AppDataBase.class,
               "schedule-db")
               .allowMainThreadQueries()
               .build();
   }

   public static synchronized AppDataBase getInstance(Context context){
       if (instance == null){
           initInstance(context);
       }
       return instance;
   }

}
