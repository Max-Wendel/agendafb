package br.ufc.quixada.dadm.variastelas.service;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;

public class ContactActivitieService {

    private final UtilService utilService = new UtilService();

    private int generator = 0;

    public String getContactName(Intent i){
        return utilService.getContactName(i);
    }

    public String getContactTel(Intent i){
        return utilService.getContactTel(i);
    }

    public String getContactEnd(Intent i){
        return utilService.getContactEnd(i);
    }

    public int getContactId(Intent i){
        return utilService.getContactId(i);
    }

    public Intent adicionar(EditText edtNome, EditText edtTel,EditText edtEnd, boolean edit, int idContatoEditar){
        Intent intent = new Intent();

        intent.putExtra("nome", edtNome.getText().toString());
        intent.putExtra("telefone", edtTel.getText().toString());
        intent.putExtra("endereco", edtEnd.getText().toString());

        int id = (edit) ? idContatoEditar : generator+1;

        intent.putExtra("id", id);

        intent.putExtra("validation",validate(edtNome.getText().toString(),edtTel.getText().toString()));

        return intent;
    }

    public void makeToast(Context context, String message){
        utilService.makeToast(context,message);
    }

    private String validate(String nome, String telefone) {
        if (!nome.isEmpty())
            if (!telefone.isEmpty())
                return "OK";
            else
                return "Não foi possível salvar o contato, por favor, insira o numero do telefone";
        else
            return "Não foi possível salvar o contato, por favor, insira o nome";
    }

    public void setGenerator(int value) {
        this.generator = value;
    }
}
