package br.ufc.quixada.dadm.variastelas.ui.acivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.util.Objects;

import br.ufc.quixada.dadm.variastelas.R;
import br.ufc.quixada.dadm.variastelas.entity.constants.AppConstants;
import br.ufc.quixada.dadm.variastelas.service.ContactActivitieService;

public class ContactActivity extends AppCompatActivity {

    EditText edtNome;
    EditText edtTel;
    EditText edtEnd;

    boolean edit;
    int idContatoEditar;

    ContactActivitieService service = new ContactActivitieService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        initValues();
        isEdit();
    }

    private void initValues() {
        edtNome = findViewById( R.id.editTextNome );
        edtTel = findViewById( R.id.editTextTel );
        edtEnd = findViewById( R.id.editTextEnd );
        edit = false;
        service.setGenerator(Integer.valueOf(Objects.requireNonNull(Objects.requireNonNull(getIntent().getExtras()).get("qtd")).toString()));
    }
    private void isEdit() {
        if( Objects.requireNonNull(getIntent().getExtras()).getInt("op") == 1 ){
            idContatoEditar = service.getContactId(getIntent());
            setContactInfo();
            edit = true;
        }
    }

    private void setContactInfo() {
        edtNome.setText( service.getContactName(getIntent()) );
        edtTel.setText( service.getContactTel(getIntent()) );
        edtEnd.setText( service.getContactEnd(getIntent()) );
    }

    public void cancelar( View view ){
        setResult( AppConstants.RESULT_CANCEL.getCodigo() );
        finish();
    }

    public void adicionar( View view ){
        Intent i = service.adicionar(edtNome,edtTel,edtEnd,edit,idContatoEditar);
        String validation = (String) Objects.requireNonNull(i.getExtras()).get("validation");
        assert validation != null;

        if (!validation.equals("OK")) {
            setResult(AppConstants.RESULT_VALIDATION_ERROR.getCodigo(), i);
        }else {
            setResult(AppConstants.RESULT_ADD.getCodigo(), i);
        }
        edit = false;
        finish();
    }

}
