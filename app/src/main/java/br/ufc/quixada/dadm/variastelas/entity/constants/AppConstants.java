package br.ufc.quixada.dadm.variastelas.entity.constants;

public enum AppConstants {
    REQUEST_ADD(11),
    REQUEST_EDIT(12),
    RESULT_ADD(21),
    RESULT_CANCEL(22),
    RESULT_VALIDATION_ERROR(99);

    int codigo;

    AppConstants(int codigo) {
        this.codigo = codigo;
    }

   public int getCodigo(){
        return this.codigo;
    }
}
