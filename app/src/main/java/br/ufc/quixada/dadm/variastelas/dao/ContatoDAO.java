package br.ufc.quixada.dadm.variastelas.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import br.ufc.quixada.dadm.variastelas.entity.Contato;

@Dao
public interface ContatoDAO {
    @Insert
    void insert(Contato... contato);

    @Update
    void update(Contato... contato);

    @Delete
    void delete(Contato... contato);

    @Query("SELECT * FROM contato")
    Contato[] loadAll();

    @Query("SELECT * FROM contato WHERE id = :id")
    Contato loadSingle(int id);
}
