package br.ufc.quixada.dadm.variastelas.service;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import br.ufc.quixada.dadm.variastelas.entity.Contato;

public class MainActivitieService {

    private final UtilService utilService = new UtilService();

    public String getContactName(Intent i){
        return utilService.getContactName(i);
    }

    public String getContactTel(Intent i){
        return utilService.getContactTel(i);
    }

    public String getContactEnd(Intent i){
        return utilService.getContactEnd(i);
    }

    public int getContactId(Intent i){
        return utilService.getContactId(i);
    }

//    public ArrayList<Contato> getRestoredText(String restoredText){
//        ArrayList<Contato> result = new ArrayList<>();
//
//        String[] contatos = restoredText.split( "_" );
//
//        for( String cont : contatos ){
//
//            Contato c = new Contato();
//
//            String[] info = cont.split( "-" );
//
//            c.setId( Integer.parseInt( info[ 0 ] ) );
//            c.setNome( info[ 1 ] );
//            c.setTelefone( info[ 2 ] );
//            c.setEndereco( info[ 3 ] );
//
//            result.add( c );
//        }
//        return result;
//    }
//
//    public String exportContactList(List<Contato> listaContatos) {
//
//        StringBuilder export = new StringBuilder();
//
//        for (Contato contato : listaContatos) {
//            export.append(contato.getFullContact());
//            export.append("_");
//        }
//
//        return export.toString();
//    }

    public Intent readyIntentToEdit(Intent intent, Contato contato){
        intent.putExtra("id", contato.getId());
        intent.putExtra("nome", contato.getNome());
        intent.putExtra("telefone", contato.getTelefone());
        intent.putExtra("endereco", contato.getEndereco());
        return intent;
    }

    public void makeToast(Context context, String message){
        utilService.makeToast(context,message);
    }
}
