package br.ufc.quixada.dadm.variastelas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import br.ufc.quixada.dadm.variastelas.entity.Contato;
import br.ufc.quixada.dadm.variastelas.entity.constants.AppConstants;
import br.ufc.quixada.dadm.variastelas.service.MainActivitieService;
import br.ufc.quixada.dadm.variastelas.ui.acivities.ContactActivity;
import br.ufc.quixada.dadm.variastelas.ui.adapters.ExpandableListAdapter;

public class MainActivity extends AppCompatActivity {

    int selected;
    ArrayList<Contato> listaContatos;
//    ArrayAdapter adapter;
    ExpandableListAdapter adapter;
//    ListView listViewContatos;
    ExpandableListView listViewContatos;

    private static final String CONTACTS_FILE = "br.ufc.quixada.dadm.variastelas.contacts_file";
    private final MainActivitieService service = new MainActivitieService();
//
//    FirebaseDatabase firebaseDatabase;
//    DatabaseReference databaseReference;

    private AppDataBase appDataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        initFireBase();
        initDatabase();
        sync();
    }

    private void sync(){
        initValues();
        initListViewContact();
        syncronizeDatabase();
    }

    private void syncronizeDatabase(){
//        databaseReference.child("Contato").addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                listaContatos.clear();
//                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
//                    Contato c = snapshot.getValue(Contato.class);
//                    listaContatos.add(c);
//                }
//                initListViewContact();
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

        Contato[] contatosArray = appDataBase.contatoDAO().loadAll();

        if (contatosArray != null){
            listaContatos.clear();
            listaContatos.addAll(Arrays.asList(contatosArray));
        }else {
            service.makeToast(this,"Não há dados para carregar");
        }

    }

    private void initDatabase() {
        appDataBase = AppDataBase.getInstance(this);
    }

//    private void initFireBase() {
//        FirebaseApp.initializeApp(this);
//        firebaseDatabase = FirebaseDatabase.getInstance();
//        firebaseDatabase.setPersistenceEnabled(true);
//        databaseReference = firebaseDatabase.getReference();
//    }

    @Override
    protected void onPause() {

        super.onPause();

        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
//        editor.putString(CONTACTS_FILE, service.exportContactList(listaContatos));
        editor.apply();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add:
                clicarAdicionar();
                break;
            case R.id.edit:
                clicarEditar();
                break;
            case R.id.delete:
                apagarItemLista();
                break;
            case R.id.settings:
                break;
            case R.id.about:
                break;
        }
        return true;
    }

    private void apagarItemLista() {
        if (listaContatos.size() > 0) {
            if (selected != -1){
                Contato contato = listaContatos.get(selected);
//                databaseReference.child("Contato").child(String.valueOf(contato.getId())).removeValue();
                appDataBase.contatoDAO().delete(contato);
                selected = -1;
            }else {
                service.makeToast(this,"É necessário selecionar um contato, para poder remove-lo");
            }
        } else {
            selected = -1;
        }
        sync();
    }

    public void clicarAdicionar() {
        Intent intent = new Intent(this, ContactActivity.class);
        intent.putExtra("qtd",listaContatos.size());
        startActivityForResult(intent, AppConstants.REQUEST_ADD.getCodigo());
    }

    public void clicarEditar() {
        Intent intent = new Intent(this, ContactActivity.class);
        intent.putExtra("op",1);
        intent.putExtra("qtd",listaContatos.size());
        Contato contato = (selected!=-1)?listaContatos.get(selected):null;
        if (contato == null){
            service.makeToast(this,"É necessário selecionar um contato, para poder editá-lo");
        }else {
            startActivityForResult(service.readyIntentToEdit(intent,contato), AppConstants.REQUEST_EDIT.getCodigo());
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstants.REQUEST_ADD.getCodigo() && resultCode == AppConstants.RESULT_ADD.getCodigo()) {
            Contato contato = new Contato(service.getContactName(data),service.getContactTel(data), service.getContactEnd(data));
            Log.d("Salvando:",contato.getFullContact());
//            listaContatos.add(contato);
//            databaseReference.child("Contato").child(String.valueOf(contato.getId())).setValue(contato);
            appDataBase.contatoDAO().insert(contato);
            sync();
        } else if (requestCode == AppConstants.REQUEST_EDIT.getCodigo() && resultCode == AppConstants.RESULT_ADD.getCodigo()) {

            for (Contato contato : listaContatos) {
                if (contato.getId() == service.getContactId(data)) {
                    contato.setNome(service.getContactName(data));
                    contato.setEndereco(service.getContactEnd(data));
                    contato.setTelefone(service.getContactTel(data));
                    Log.d("Salvando:",contato.getFullContact());
//                    databaseReference.child("Contato").child(String.valueOf(contato.getId())).setValue(contato);
                    appDataBase.contatoDAO().update(contato);
                }
            }

            sync();

        } //Retorno da tela de contatos com um conteudo para ser adicionado
        //Na segunda tela, o usuario clicou no botão ADD
        else if (resultCode == AppConstants.RESULT_CANCEL.getCodigo()) {
            service.makeToast(this,"Cancelado");
        }else if (requestCode == AppConstants.REQUEST_ADD.getCodigo() && resultCode == AppConstants.RESULT_VALIDATION_ERROR.getCodigo()){
            assert data != null;
            service.makeToast(this,(String) Objects.requireNonNull(data.getExtras()).get("validation"));
        }
    }

    private void initValues() {
        selected = -1;
        listaContatos = new ArrayList<>();
    }

    private void initListViewContact() {
        adapter = new ExpandableListAdapter(this, listaContatos);

        listViewContatos = findViewById(R.id.expandableListView);
        listViewContatos.setAdapter(adapter);
        listViewContatos.setSelector(android.R.color.holo_blue_light);
        listViewContatos.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                selected = groupPosition;
                return false;
            }
        });
    }

}
